﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.Data.SqlClient;
using System.IO;
using System.Transactions;

namespace SynVerifyTolls
{
    public partial class Form1 : Form
    {
        string verify_tbaleName = ConfigurationManager.AppSettings["verify_tableName"].ToString();//获取表名
        int sleep_time =Convert.ToInt32(ConfigurationManager.AppSettings["sleep_time"]);//校验时间间隔
        int verify_time =Convert.ToInt32(ConfigurationManager.AppSettings["verify_time"]);//校验多久之前的数据
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string localConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_local"].ToString();//fufuzhu
        string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorLog\\";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successLog\\";//校验日志记录路径
        string sqlLogPath = System.Environment.CurrentDirectory + "\\sqlLog\\";//校验sql语句记录路径
        System.Timers.Timer timer_yesterday = new System.Timers.Timer();
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        int DBtype = Convert.ToInt32(ConfigurationManager.AppSettings["DBtype"]);

        public Form1()
        {
            InitializeComponent();
            sleep_time = sleep_time * 1000 * 60;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            Thread thread = new Thread(new ThreadStart(RunVerify));
            thread.Start();

            timer_yesterday.AutoReset = false;
            timer_yesterday.Interval = 1000;
            timer_yesterday.Elapsed += new System.Timers.ElapsedEventHandler(RunVerify_yesterday);
            timer_yesterday.Start();

            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();
        }

        void RunVerify()
        {
            string identityid = "";
            DateTime begindate = dateTimePicker1.Value.Date;

            if (begindate == DateTime.Now.Date)//如果校验的日期是当天的话则定时校验，只校验设定值的时间段内的数据
            {
                while (true)
                {
                    try
                    {
                        //获取in表汇总记录里的最大时间
                        string strin = "select max(add_time) as maxaddtime from dt_user_in_verify_test";
                        var intable = SqlDbHelper.GetQuery(strin,localConnStr);
                        DateTime inenddate = Convert.ToDateTime(intable.Rows[0]["maxaddtime"]).AddMinutes(-verify_time);
                        FillMsg2("校验结果时间点:" + inenddate);

                        //获取out表汇总记录里的最大时间
                        string strout = "select max(add_time) as maxaddtime from dt_user_out_verify_test";
                        var outtable = SqlDbHelper.GetQuery(strout, localConnStr);
                        DateTime outenddate = Convert.ToDateTime(outtable.Rows[0]["maxaddtime"]).AddMinutes(-verify_time);


                        //获取orders表汇总记录里的最大时间
                        string strorder = "select max(add_time) as maxaddtime from dt_lottery_verify_test";
                        var ordertable = SqlDbHelper.GetQuery(strorder, localConnStr);
                        DateTime orderenddate = Convert.ToDateTime(ordertable.Rows[0]["maxaddtime"]).AddMinutes(-verify_time);

                        //获取winning表汇总记录里的最大时间
                        string strwining = "select max(add_time) as maxaddtime from dt_lottery_winning_verify_test";
                        var winningtable = SqlDbHelper.GetQuery(strwining, localConnStr);
                        DateTime winenddate = Convert.ToDateTime(winningtable.Rows[0]["maxaddtime"]).AddMinutes(-verify_time);

                        //获取user_get_points表汇总记录里的最大时间
                        string strpoint = "select max(add_time) as maxaddtime from dt_user_get_point_verify_test";
                        var pointtable = SqlDbHelper.GetQuery(strpoint, localConnStr);
                        DateTime pointenddate = Convert.ToDateTime(pointtable.Rows[0]["maxaddtime"]).AddMinutes(-verify_time);

                        DateTime enddate = DateTime.Now;
                        string strsql = "select identityid from dt_tenant where DBtype='" + DBtype + "' order by identityid";//获取所有的站长
                        var identitytable = SqlDbHelper.GetQuery(strsql,sql_share);
                        for (int i = 0; i < identitytable.Rows.Count; i++)
                        {
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                            {
                                Thread.Sleep(1000 * 60 * 60 * 3);
                                continue;
                            }
                            identityid = identitytable.Rows[i]["identityid"].ToString();
                            FillMsg1("正在校验站长:" + identityid + "...");
                            string[] tableName = verify_tbaleName.Split(',');
                            string errortable = "";
                            try
                            {
                                foreach (var tablename in tableName)
                                {
                                    errortable = tablename;
                                    if (tablename == "dt_user_in_account")
                                    {
                                        begindate = inenddate.Date;
                                        enddate = inenddate;
                                        VerifyIn(identityid, begindate, inenddate);//校验dt_user_in_account
                                        System.Threading.Thread.Sleep(800);
                                    }

                                    if (tablename == "dt_user_out_account")
                                    {
                                        begindate = outenddate.Date;
                                        enddate = outenddate;
                                        VerifyOut(identityid, begindate, outenddate);
                                        System.Threading.Thread.Sleep(800);
                                    }
                                    begindate = DateTime.Now.Date;
                                    if (tablename == "dt_lottery_orders")
                                    {
                                        begindate = orderenddate.Date;
                                        enddate = orderenddate;
                                        VerifyLottery(identityid, begindate, orderenddate);
                                        System.Threading.Thread.Sleep(800);
                                    }
                                    if (tablename == "dt_lottery_winning_record")
                                    {
                                        begindate = winenddate.Date;
                                        winenddate = System.DateTime.Now;
                                        enddate = winenddate;
                                        VerifyWinning(identityid, begindate, winenddate);
                                        System.Threading.Thread.Sleep(800);
                                    }
                                    if (tablename == "dt_user_get_point_record")
                                    {
                                        begindate = pointenddate.Date;
                                        enddate = pointenddate;
                                        VerifyPoint(identityid, begindate, pointenddate);
                                        System.Threading.Thread.Sleep(800);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                FillErrorMsg(errortable + "站长:" + identityid + "\r\n" + "校验时间段:" + begindate + "---" + enddate + "\r\n" + ex);
                                WriteErrorLog(DateTime.Now.ToString() + "\r\n" + errortable + "校验时间段:" + begindate + "---" + enddate, "站长" + identityid + "校验失败");
                            }
                            finally
                            {
                                FillMsg1("校验完毕..");
                                WriteSuccessLog("校验站长:" + identityid + "完毕..." + DateTime.Now.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        FillErrorMsg("错误日志:" + ex);
                        WriteErrorLog("错误时间:" + DateTime.Now.ToString(), ex.ToString());
                    }
                    finally
                    {
                        Thread.Sleep(sleep_time);//线程睡眠时间
                    }
                }

            }
            else //校验的日期不是当天的话则校验全天的数据,只执行一次
            {
                identityid = textBox1.Text;
                DateTime enddate = dateTimePicker1.Value.Date.AddDays(1);
                FillMsg1("校验已开启...");
                if (string.IsNullOrEmpty(identityid) == false)//站长不为空则只针对某个站长进行校验
                {
                    FillMsg1("正在校验站长:" + identityid + "...");

                    string[] tableName = verify_tbaleName.Split(',');
                    string errortable = "";
                    try
                    {
                        foreach (var tablename in tableName)
                        {
                            errortable = tablename;
                            if (tablename == "dt_user_in_account")
                            {
                                VerifyIn(identityid, begindate, enddate);//校验dt_user_in_account
                            }
                            else if (tablename == "dt_user_out_account")
                            {
                                VerifyOut(identityid, begindate, enddate);
                            }
                            //else if (tablename == "dt_lottery_orders")
                            //{
                            //    VerifyLottery(identityid, begindate, enddate);
                            //}
                            else if (tablename == "dt_lottery_winning_record")
                            {
                                VerifyWinning(identityid, begindate, enddate);
                            }
                            else if (tablename == "dt_user_get_point_record")
                            {
                                VerifyPoint(identityid, begindate, enddate);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        FillErrorMsg(errortable + "站长" + identityid + "\r\n" + "校验时间段:" + begindate + "---" + enddate + "\r\n" + ex);
                        WriteErrorLog(DateTime.Now.ToString() + "\r\n" + errortable + "校验时间段:" + begindate + "---" + enddate, "站长" + identityid + "校验失败");
                    }
                    finally
                    {
                        FillMsg1("校验完毕..." + DateTime.Now.ToString());
                    }
                }
                else //站长为空则只针对所有站长进行校验
                {
                    string strsql = "select identityid from dt_tenant where DBtype='" + DBtype + "' order by identityid";
                    var identitytable = SqlDbHelper.GetQuery(strsql, sql_share);
                    for (int i = 0; i < identitytable.Rows.Count; i++)
                    {
                        identityid = identitytable.Rows[i]["identityid"].ToString();
                        FillMsg1("正在校验站长:" + identityid + "...");
                        string[] tableName = verify_tbaleName.Split(',');
                        string errortable = "";
                        try
                        {
                            foreach (var tablename in tableName)
                            {
                                errortable = tablename;
                                if (tablename == "dt_user_in_account")
                                {
                                    VerifyIn(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_user_out_account")
                                {
                                    VerifyOut(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_lottery_orders")
                                {
                                    VerifyLottery(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_lottery_winning_record")
                                {
                                    VerifyWinning(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_user_get_point_record")
                                {
                                    VerifyPoint(identityid, begindate, enddate);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg(errortable + "站长" + identityid + "\r\n" + "校验时间段:" + begindate + "---" + enddate + "\r\n" + ex);
                            WriteErrorLog(DateTime.Now.ToString() + "\r\n" + errortable + "校验时间段:" + begindate + "---" + enddate, "站长" + identityid + "校验失败");
                        }
                        finally
                        {
                            FillMsg1("校验完毕..." + DateTime.Now.ToString());
                        }
                    }
                }
            }
        }

        private void RunVerify_yesterday(object sender, System.Timers.ElapsedEventArgs e)
        {
            string identityid = "";
            DateTime begindate = System.DateTime.Now.AddDays(-1).Date;
            DateTime enddate = System.DateTime.Now.Date;
            try
            {
                if (System.DateTime.Now.ToString("HH:mm") == "00:30")
                {
                    string strsql = "select identityid from dt_tenant where DBtype='" + DBtype + "' order by identityid";//获取所有的站长
                    var identitytable = SqlDbHelper.GetQuery(strsql, sql_share);
                    for (int i = 0; i < identitytable.Rows.Count; i++)
                    {
                        identityid = identitytable.Rows[i]["identityid"].ToString();
                        FillMsg1("yesterday正在校验站长:" + identityid + "...");
                        WriteSuccessLog("yesterday正在校验站长:" + identityid + "...");
                        string[] tableName = verify_tbaleName.Split(',');
                        string errortable = "";
                        try
                        {
                            foreach (var tablename in tableName)
                            {
                                errortable = tablename;

                                if (tablename == "dt_user_in_account")
                                {
                                    begindate = enddate.AddDays(-1).Date;
                                    VerifyIn(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_user_out_account")
                                {
                                    begindate = enddate.Date;
                                    VerifyOut(identityid, begindate, enddate);
                                }
                                //else if (tablename == "dt_lottery_orders")
                                //{
                                //    begindate = enddate.AddDays(-1).Date;
                                //    VerifyLottery(identityid, begindate, enddate);
                                //}
                                else if (tablename == "dt_lottery_winning_record")
                                {
                                    begindate = enddate.AddDays(-1).Date;
                                    VerifyWinning(identityid, begindate, enddate);
                                }
                                else if (tablename == "dt_user_get_point_record")
                                {
                                    begindate = enddate.AddDays(-1).Date;
                                    VerifyPoint(identityid, begindate, enddate);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg(errortable + "yesterday站长" + identityid + "\r\n" + "校验时间段:" + begindate + "---" + enddate + "\r\n" + ex);
                            WriteErrorLog(DateTime.Now.ToString() + "\r\n" + errortable + "yesterday校验时间段:" + begindate + "---" + enddate, "站长" + identityid + "校验失败");
                        }
                        finally
                        {
                            FillMsg1("yesterday校验完毕..");
                            WriteSuccessLog("yesterday校验站长:" + identityid + "完毕..." + DateTime.Now.ToString());
                        }
                    }
                }
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_yesterday.Start();
            }
        }

        void VerifyIn(string identityid, DateTime begindate, DateTime enddate)
        {
            string azurestrsql = "";
            string reportsql = "";
            string tableName = "dt_user_in_account";
            string produceName = "dsp_user_in_account_synSum_test";
            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 10){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@flog1", SqlDbType.Int){Value=1},
                new SqlParameter("@state1", SqlDbType.Int){Value=1},
                new SqlParameter("@type1", SqlDbType.Int){Value=1}
            };

            List<SqlParameter> ReportSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 50){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@state1", SqlDbType.Int){Value=1},
                new SqlParameter("@type1", SqlDbType.Int){Value=1}
            };

            //充值笔数
            azurestrsql = "select id from dt_user_in_account where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type=@type1 and state=@state1 and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_in_verify_test with(nolock) where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type=@type1 and state=@state1";
            RunPro(azurestrsql, reportsql, "充值金额", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());
        }
        void VerifyOut(string identityid, DateTime begindate, DateTime enddate)
        {
            string azurestrsql = "";
            string reportsql = "";
            string tableName = "dt_user_out_account";
            string produceName = "dsp_user_out_account_synSum_test";
            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 10){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@flog1", SqlDbType.Int){Value=1},
                new SqlParameter("@state1", SqlDbType.Int){Value=1},
                new SqlParameter("@state3", SqlDbType.Int){Value=3},
                new SqlParameter("@type3", SqlDbType.Int){Value=3},
                new SqlParameter("@type9", SqlDbType.Int){Value=9},
                new SqlParameter("@type10", SqlDbType.Int){Value=10}
            };

            List<SqlParameter> ReportSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 50){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@state1", SqlDbType.Int){Value=1},
                new SqlParameter("@state3", SqlDbType.Int){Value=3},
                new SqlParameter("@type3", SqlDbType.Int){Value=3},
                new SqlParameter("@type9", SqlDbType.Int){Value=9},
                new SqlParameter("@type10", SqlDbType.Int){Value=10}
            };
            //人工提出
            azurestrsql = "select id from dt_user_out_account where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type3 and state=@state1 and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_out_verify_test with(nolock) where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type3 and state=@state1";
            RunPro(azurestrsql, reportsql, "人工提出", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());

            //误存提出
            azurestrsql = "select id from dt_user_out_account where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type9 and state=@state1 and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_out_verify_test with(nolock) where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type9 and state=@state1";
            RunPro(azurestrsql, reportsql, "误存提出", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());

            //行政提出
            azurestrsql = "select id from dt_user_out_account where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type10 and state=@state1 and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_out_verify_test with(nolock) where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type10 and state=@state1";
            RunPro(azurestrsql, reportsql, "行政提出", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());

            //拒绝金额
            azurestrsql = "select id from dt_user_out_account where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type3 and state=@state3 and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_out_verify_test with(nolock) where identityid=@identityid and cztime>=@begindate and cztime<@enddate and type2=@type3 and state=@state3";
            RunPro(azurestrsql, reportsql, "拒绝金额", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());
        }
        void VerifyLottery(string identityid, DateTime begindate, DateTime enddate)
        {
            string azurestrsql = "";
            string reportsql = "";
            string tableName = "dt_lottery_orders";
            string produceName = "dsp_lottery_orders_synSum_test";
            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 10){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@flog1", SqlDbType.Int){Value=1},
                new SqlParameter("@openstate1", SqlDbType.Int){Value=1},
                new SqlParameter("@openstate2", SqlDbType.Int){Value=2}
            };

            List<SqlParameter> ReportSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 50){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@openstate1", SqlDbType.Int){Value=1},
                new SqlParameter("@openstate2", SqlDbType.Int){Value=2}
            };
            //投注金额
            azurestrsql = "select id from dt_lottery_orders where identityid=@identityid and add_time>=@begindate and add_time<@enddate and openstate in(@openstate1,@openstate2) and flog=@flog1 OPTION (RECOMPILE)";
            reportsql = "select id from dt_lottery_verify_test with(nolock) where identityid=@identityid and add_time>=@begindate and add_time<@enddate";
            RunPro(azurestrsql, reportsql, "投注金额", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());
        }
        void VerifyWinning(string identityid, DateTime begindate, DateTime enddate)
        {
            string azurestrsql = "";
            string reportsql = "";
            string tableName = "dt_lottery_winning_record";
            string produceName = "dsp_lottery_winning_synSum_test";
            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 10){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@flog1", SqlDbType.Int){Value=1}
            };

            List<SqlParameter> ReportSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 50){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate}
            };
            //中奖金额
            azurestrsql = "select id from dt_lottery_winning_record where identityid=@identityid and add_time>=@begindate and add_time<@enddate and flog=@flog1 OPTION (RECOMPILE)";
            reportsql = "select id from dt_lottery_winning_verify_test with(nolock) where identityid=@identityid and add_time>=@begindate and add_time<@enddate";
            RunPro(azurestrsql, reportsql, "中奖金额", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());
        }
        void VerifyPoint(string identityid, DateTime begindate, DateTime enddate)
        {
            string azurestrsql = "";
            string reportsql = "";
            string tableName = "dt_lottery_winning_record";
            string produceName = "dsp_user_get_point_synSum_test";
            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 10){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate},
                new SqlParameter("@flog1", SqlDbType.Int){Value=1}
            };

            List<SqlParameter> ReportSqlParamter = new List<SqlParameter>()
            {
                new SqlParameter("@identityid", SqlDbType.VarChar, 50){Value=identityid},
                new SqlParameter("@begindate", SqlDbType.DateTime2){Value=begindate},
                new SqlParameter("@enddate", SqlDbType.DateTime2){Value=enddate}
            };
            //返点金额
            azurestrsql = "select id from dt_user_get_point_record where identityid=@identityid and add_time>=@begindate and add_time<@enddate and user_id in(select id from dt_users where identityid=@identityid and flog=@flog1) OPTION (RECOMPILE)";
            reportsql = "select id from dt_user_get_point_verify_test with(nolock) where identityid=@identityid and add_time>=@begindate and add_time<@enddate";
            RunPro(azurestrsql, reportsql, "返点金额", tableName, produceName, AzureSqlParamter.ToArray(), ReportSqlParamter.ToArray());
        }

        /// <summary>
        /// 比對數量
        /// </summary>
        /// <param name="azurestrsql">线上取数据</param>
        /// <param name="reportsql">报表服务器取数据</param>
        /// <param name="typeMsg">校验的各字段</param>
        /// <param name="tableflg">1：in表，2：out表</param>
        void RunPro(string azurestrsql, string reportsql, string typeMsg, string tableName, string prodruceName, SqlParameter[] AzureSqlParamter, SqlParameter[] ReportSqlParamter)
        {
            DataTable azuredt = null;
            DataTable reportdt = null;
            azuredt = SqlDbHelper.GetQuery(azurestrsql, AzureSqlParamter, reportConnStr);
            reportdt = SqlDbHelper.GetQuery(reportsql, ReportSqlParamter, localConnStr);
            if (azuredt.Rows.Count != reportdt.Rows.Count)
            {
                //取差集
                var dataRows = azuredt.AsEnumerable().Except(reportdt.AsEnumerable(), DataRowComparer.Default);
                if (dataRows.Count() > 0)
                {
                    FillMsg2(typeMsg + "校验出数据缺" + dataRows.Count() + "条");
                    StringBuilder sb = new StringBuilder();
                    int i = 0;
                    foreach (var item in dataRows)
                    {
                        i++;
                        string id = item.ItemArray[0].ToString();
                        sb.Append(id);
                        if (i != dataRows.Count())
                            sb.Append(",");
                    }
                    string columns = "*";
                    if (tableName == "dt_user_in_account")
                        columns = "id,identityid,user_id,money,type,type2,state,add_time,cztime,add_time2,SourceName,lockid";
                    else if (tableName == "dt_user_out_account")
                        columns = "id,identityid,user_id,money,type,type2,state,add_time,cztime,SourceName,lockid";
                    string verifySql = string.Format("select " + columns + " from {0} where id in({1})", tableName, sb);
                    runVerifySynSum(typeMsg, dataRows.Count(), verifySql, prodruceName);//补数据
                }
                else
                {
                    dataRows = reportdt.AsEnumerable().Except(azuredt.AsEnumerable(), DataRowComparer.Default);
                    FillMsg2(typeMsg + "校验出数据多汇总" + dataRows.Count() + "条");
                    StringBuilder sb = new StringBuilder();
                    int i = 0;
                    foreach (var item in dataRows)
                    {
                        i++;
                        string id = item.ItemArray[0].ToString();
                        sb.Append(id);
                        if (i != dataRows.Count())
                            sb.Append(",");
                    }
                    WriteSqlLog(typeMsg + "校验出数据多汇总" + dataRows.Count() + "条,id:" + sb.ToString());
                }
            }
        }

        /// <summary>
        /// 補數據
        /// </summary>
        /// <param name="typemsg">校验字段</param>
        /// <param name="count">校验条数</param>
        /// <param name="verifySql">校验的sql语句</param>
        /// <param name="ProdruceName">存储过程名</param>
        void runVerifySynSum(string typemsg, int count, string verifySql, string ProdruceName)
        {
            try
            {
                var table = SqlDbHelper.GetQuery(verifySql, reportConnStr);
                if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                {
                    table.Columns.Remove("lockid");
                    TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                    int result = SqlDbHelper.RunInsert(table, "isVerify", ProdruceName, localConnStr);
                    if (result == 1)
                    {
                        FillMsg2(typemsg + "補數據失败,重新汇总中...");
                        Thread.Sleep(2000);
                        runVerifySynSum(typemsg, count, verifySql, ProdruceName);
                    }
                    else
                    {
                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                        string dateDiff = ts.Seconds.ToString() + "秒";
                        FillMsg2(typemsg + "成功補數據" + count + "条数据,耗时:" + dateDiff);
                        WriteSuccessLog(typemsg + "成功補數據" + count + "条数据,sql语句:" + verifySql + ",耗时:" + dateDiff);
                    }
                }
            }
            catch (Exception ex)
            {
                FillErrorMsg(typemsg + "補數據失败:" + ex.ToString());
                WriteSqlLog(typemsg + ":" + ex.ToString());
            }
        }

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\t\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 日志记录
        private object obj = new object();
        void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                string logRoadError1 = errorLogPath + DateTime.Now.Date.ToString("yyyyMMdd") + ".txt";
                if (!File.Exists(logRoadError1))
                {
                    FileStream fs1 = new FileStream(logRoadError1, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(logRoadError1, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        private object obj1 = new object();
        void WriteSuccessLog(string msgex)
        {
            lock (obj1)
            {
                string logRoadSuccess1 = successLogPath + DateTime.Now.Date.ToString("yyyyMMdd") + ".txt";
                if (!File.Exists(logRoadSuccess1))
                {
                    FileStream fs1 = new FileStream(logRoadSuccess1, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(logRoadSuccess1, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        private object obj2 = new object();
        void WriteSqlLog(string msgex)
        {
            lock (obj2)
            {
                string logRoadsql1 = sqlLogPath + DateTime.Now.Date.ToString("yyyyMMdd") + ".txt";
                if (!File.Exists(logRoadsql1))
                {
                    FileStream fs1 = new FileStream(logRoadsql1, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(logRoadsql1, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "00")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                errorBox.Invoke(rb);

            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    errorBox.Clear();

                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
            SqlDependency.Stop(localConnStr);
        }      
    }
}
